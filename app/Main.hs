module Main (main) where

import Data.Aeson (encode)
import Network.Socket (Socket)
import Data.ByteString.Lazy (toStrict)

import Model
import Http

things = [
  Thing "A" 100,
  Thing "B" 200,
  Thing "C" 300
         ]

main :: IO ()
main = runTCPServer Nothing "3000" server

server :: Socket -> IO ()
server s = do
  request <- recvRequest s
  sendResponse s $ (toStrict . encode) things
