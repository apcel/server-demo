{-# LANGUAGE DeriveGeneric #-}
module Model where

import GHC.Generics
import Data.Aeson

data Thing = Thing {
  foo :: String,
  bar :: Int
  } deriving (Generic, Show)

instance ToJSON Thing
instance FromJSON Thing
