{-# LANGUAGE OverloadedStrings #-}
module Http (runTCPServer, recvRequest, sendResponse) where

import Control.Concurrent (forkFinally)
import qualified Control.Exception as E
import Control.Monad (unless, forever, void)
import qualified Data.ByteString as S
import Data.ByteString.Lazy (toStrict)
import Data.ByteString.Char8 (unpack, pack)
import Network.Socket
import Network.Socket.ByteString (recv, sendAll)
import HttpRequestParser
import Text.Parsec

sendResponse :: Socket -> S.ByteString -> IO ()
sendResponse s body = sendAll s response
  where
    response = "HTTP/1.1 200 OK\n" <> contentType <> contentLength <> "\n" <> body
    contentLength = "Content-Length: " <> (pack . show . S.length) body <> "\n"
    contentType = "Content-Type: application/json\n"

recvRequest :: Socket -> IO (Either ParseError HttpRequest)
recvRequest s = recvAll s >>= return.parseHTTP

parseHTTP :: S.ByteString -> Either ParseError HttpRequest
parseHTTP = (parse p_request "").unpack

recvAll :: Socket -> IO S.ByteString
recvAll s = recvAll' "" s
  where
    recvAll' :: S.ByteString -> Socket -> IO S.ByteString
    recvAll' result s = do
      msg <- recv s 1024
      if (S.length msg <= 1024)
      then return msg
      else if (not . S.null) msg
          then recvAll' (result <> msg) s
          else return result

-- from the "network-run" package.
runTCPServer :: Maybe HostName -> ServiceName -> (Socket -> IO a) -> IO a
runTCPServer mhost port server = withSocketsDo $ do
    addr <- resolve
    E.bracket (open addr) close loop
  where
    resolve = do
        let hints = defaultHints {
                addrFlags = [AI_PASSIVE]
              , addrSocketType = Stream
              }
        head <$> getAddrInfo (Just hints) mhost (Just port)
    open addr = do
        sock <- socket (addrFamily addr) (addrSocketType addr) (addrProtocol addr)
        setSocketOption sock ReuseAddr 1
        withFdSocket sock $ setCloseOnExecIfNeeded
        bind sock $ addrAddress addr
        listen sock 1024
        return sock
    loop sock = forever $ do
        (conn, _peer) <- accept sock
        void $ forkFinally (server conn) (const $ gracefulClose conn 5000)
